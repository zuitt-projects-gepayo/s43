console.log("Hello")

let posts = [];
let count = 1;


document.querySelector("#form-add-post").addEventListener("submit", (e)=>{
    
    e.preventDefault();

    posts.push({
        id: count,
        title: document.querySelector("#txt-title").value,
        body: document.querySelector("#txt-body").value
    })

    count ++
    showPosts(posts);

})

const showPosts = (posts) => {

    let postEntries = '';

    posts.forEach((post) => {
        console.log(post)
        postEntries +=  `
        
        <div id="post-${post.id}">

            <h3 id="post-title-${post.id}">${post.title}</h3>

            <p id="post-body-${post.id}">${post.body}</p>
        </div>

        <button onclick="editPost('${post.id}')">Edit</button>

        <button onclick="deletePost('${post.id}')">Delete</button>

        `;
    });
    document.querySelector("#div-post-entries").innerHTML = postEntries;
};

//DELETE POST
const deletePost = (id) => {

    for(let i=0; i < id; i++)
    {
        if(posts[i].id.toString() === id) {

           posts.splice(i,1)

           showPosts(posts)
           break;
        }
    }
};








//edit post function
const editPost = (id) => {

    let title = document.querySelector(`#post-title-${id}`).innerHTML;

    let body = document.querySelector(`#post-body-${id}`).innerHTML;

    document.querySelector("#txt-edit-id").value=id;

    document.querySelector("#txt-edit-title").value = title;

    document.querySelector("#txt-edit-body").value = body;
};

//Update POST
document.querySelector("#form-edit-post").addEventListener("submit", (e) => {

    e.preventDefault();


    for(let i=0; i < posts.length; i++)
    {
        if(posts[i].id.toString() === document.querySelector("#txt-edit-id").value) {

            posts[i].body = document.querySelector("#txt-edit-body").value;

            showPosts(posts)
            alert("Post Successfully Updated")

            break;
        }
    }
});


